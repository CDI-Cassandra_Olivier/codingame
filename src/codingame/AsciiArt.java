package codingame;

import java.util.Scanner;

public class AsciiArt {
	  public static void main(String args[]) {
	        Scanner in = new Scanner(System.in);
	        int L = in.nextInt();
	        int H = in.nextInt();
	        String S="*";
	        
	        if (in.hasNextLine()) {
	            in.nextLine();
	        }
	        String T = in.nextLine();
	        for (int i = 0; i < H; i++) {
	            String ROW = in.nextLine();
	            
	            for(int j=0;j<L;j++){
	                System.err.println("*");
	                }
	            System.err.println("**");
	        }
	        
	        

	        // Write an action using System.out.println()
	        // To debug: System.err.println("Debug messages...");

	        System.out.println("answer");
	    }
	    
	}